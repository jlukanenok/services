package com.Controllers;

import com.Models.Player;
import com.Services.PlayerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/player")
@Api(value="Services", description="Operations related to players repository")
public class PlayerController {

    @Autowired
    PlayerService playerService;

    @ApiOperation(value = "Get player by id", response = Iterable.class)
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Player GetById(@PathVariable Integer id){
        return playerService.GetById(id);
    }

    @ApiOperation(value = "Get player by last name, upper/smaller case doesn't matter, we'll handle it", response = Iterable.class)
    @RequestMapping(value = "/lastname/{lastName}", method = RequestMethod.GET)
    Player GetByLastName(@PathVariable String lastName){
        return playerService.GetByLastName(lastName);
    }

    @ApiOperation(value = "Get all players from team", response = Iterable.class)
    @RequestMapping(value = "/team/{teamId}", method = RequestMethod.GET)
    public List<Player> GetByTeam(@PathVariable Integer teamId){
        return playerService.GetByTeam(teamId);
    }


    @RequestMapping(value = "/all", method = RequestMethod.GET)
    @ApiOperation(value = "Get all players in database", response = Iterable.class)
    public List<Player> GetAll(){
        return playerService.GetAll();
    }


}
