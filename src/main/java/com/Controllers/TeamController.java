package com.Controllers;


import com.Models.Team;
import com.Services.TeamService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/team")
@Api(value="Services", description="Operations related to team repository")
public class TeamController {

    @Autowired
    TeamService teamService;

    @ApiOperation(value = "Get team by id", response = Iterable.class)
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Team GetById(@PathVariable Integer id){
        return teamService.GetById(id);
    }

    @ApiOperation(value = "Get team by name, upper/smaller case doesn't matter, we'll handle it", response = Iterable.class)
    @RequestMapping(value = "/name/{name}", method = RequestMethod.GET)
    public Team GetByName(@PathVariable String name){
        return teamService.GetByName(name);
    }

    @ApiOperation(value = "Get all teams", response = Iterable.class)
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<Team> GetAll(){
        return teamService.GetAll();
    }
}
