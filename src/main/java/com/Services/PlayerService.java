package com.Services;


import com.Models.Player;
import com.Repositories.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PlayerService {

    @Autowired
    private PlayerRepository playerRepository;

    public Player GetByLastName(String lastName){
        return playerRepository.GetByLastName(lastName);
    }


    public Player GetById(Integer id){
        return playerRepository.GetById(id);

    }

    public List<Player> GetByTeam(Integer teamId){
        return playerRepository.GetByTeam(teamId);
    }


    public List<Player> GetAll(){
        return playerRepository.GetAll();
    }

}
