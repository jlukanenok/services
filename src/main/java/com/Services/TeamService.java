package com.Services;

import com.Models.Player;
import com.Models.Team;
import com.Repositories.PlayerRepository;
import com.Repositories.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TeamService {

    @Autowired
    TeamRepository teamRepository;

    @Autowired
    PlayerRepository playerRepository;

    public Team GetByName(String name){

       Team team = teamRepository.GetByName(name);

       if(team != null){
           List<Player> teamPlayers = playerRepository.GetByTeam(team.getId());
           team.setPlayers(teamPlayers);
           return team;
       }
       else {
           return null;
       }
    }

    public Team GetById(Integer id){

        Team team = teamRepository.GetById(id);
        if(team != null){
            List<Player> teamPlayers = playerRepository.GetByTeam(team.getId());
            team.setPlayers(teamPlayers);
            return team;
        }
        else {
            return null;
        }
    }

    public List<Team> GetAll(){
        List<Team> teams = teamRepository.GetAll();
        for(Team team : teams){
           team.setPlayers(playerRepository.GetByTeam(team.getId()));
        }
        return teams;
    }
}
