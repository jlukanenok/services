package com.Models;

import io.swagger.annotations.ApiModelProperty;

import java.util.List;

public class Team {

    @ApiModelProperty(notes = "The database generated product ID")
    private int id;
    @ApiModelProperty(notes = "Team name")
    private String name;
    @ApiModelProperty(notes = "Team league")
    private String league;
    @ApiModelProperty(notes = "List of team players")
    private List<Player> players;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getLeague() {
        return league;
    }

    public void setLeague(String league) {
        this.league = league;
    }


    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }
}
