package com.Models;


import io.swagger.annotations.ApiModelProperty;

public class Player {

    @ApiModelProperty(notes = "The database generated product ID")
    private int id;
    @ApiModelProperty(notes = "Player first name")
    private String firstName;
    @ApiModelProperty(notes = "Player last name")
    private String lastName;
    @ApiModelProperty(notes = "TeamId where player belongs to")
    private int teamId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }


    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getTeamId() {
        return teamId;
    }

    public void setTeamId(int teamId) {
        this.teamId = teamId;
    }
}
