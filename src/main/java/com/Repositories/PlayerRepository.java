package com.Repositories;

import com.Models.Player;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class PlayerRepository {

    private ArrayList<Player> players;

    public PlayerRepository(){
        AddData();
    }

    public Player GetByLastName(String lastName){
        Player playerToReturn = players.stream()
                .filter(player -> lastName.toLowerCase().equals(player.getLastName().toLowerCase()))
                .findAny()
                .orElse(null);
        return playerToReturn;
    }


    public Player GetById(Integer id){
        Player playerToReturn = players.stream()
                .filter(player -> id.equals(id))
                .findAny()
                .orElse(null);
        return playerToReturn;
    }

    public List<Player> GetByTeam(Integer teamId){
        List<Player> teamPlayers = players.stream().filter(x->x.getTeamId() == teamId).collect(Collectors.toList());
        return teamPlayers;
    }

    public List<Player> GetAll(){
        return players;
    }

    private void AddData(){

        players = new ArrayList<>();

        Player messi = new Player();
        messi.setId(1);
        messi.setFirstName("Lionel");
        messi.setLastName("Messi");
        messi.setTeamId(1);

        Player dembele = new Player();
        dembele.setId(4);
        dembele.setFirstName("Ousmane");
        dembele.setLastName("Dembele");
        dembele.setTeamId(1);

        Player kane = new Player();
        kane.setId(2);
        kane.setFirstName("Harry");
        kane.setLastName("Kane");
        kane.setTeamId(2);

        Player alli = new Player();
        alli.setId(5);
        alli.setFirstName("Dele");
        alli.setLastName("Alli");
        alli.setTeamId(2);

        Player ronaldo = new Player();
        ronaldo.setId(3);
        ronaldo.setFirstName("Cristiano");
        ronaldo.setLastName("Ronaldo");
        ronaldo.setTeamId(3);

        Player dybala = new Player();
        dybala.setId(6);
        dybala.setFirstName("Paulo");
        dybala.setLastName("Dybala");
        dybala.setTeamId(3);

        players.add(dybala);
        players.add(messi);
        players.add(ronaldo);
        players.add(kane);
        players.add(alli);
        players.add(dembele);
    }
}
