package com.Repositories;

import com.Models.Team;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class TeamRepository {

    private List<Team> teams;

    public TeamRepository(){
        AddData();
    }

    public Team GetByName(String name){

        Team teamToReturn = teams.stream()
                .filter(team -> name.toLowerCase().equals(team.getName().toLowerCase()))
                .findAny()
                .orElse(null);
        return teamToReturn;
    }

    public Team GetById(Integer id){

        Team teamToReturn = teams.stream()
                .filter(team -> id.equals(team.getId()))
                .findAny()
                .orElse(null);
        return teamToReturn;
    }

    public List<Team> GetAll(){
        return teams;
    }

    private void AddData(){

        teams = new ArrayList<>();

        Team barcelona = new Team();
        barcelona.setId(1);
        barcelona.setLeague("La Liga");
        barcelona.setName("Barcelona");

        Team tottenham= new Team();
        tottenham.setId(2);
        tottenham.setLeague("Premier League");
        tottenham.setName("Tottenham Hotspurs");

        Team juventus = new Team();
        juventus.setId(3);
        juventus.setLeague("Series A");
        juventus.setName("Juventus");

        teams.add(barcelona);
        teams.add(tottenham);
        teams.add(juventus);
    }
}
